db = db.getSiblingDB('db_development');
db.createUser(
  {
    user: 'devuser',
    pwd: 'devpass',
    roles: [{ role: 'readWrite', db: 'db_development' }],
  },
);
db.createCollection('users');

db = db.getSiblingDB('db_test');
db.createUser(
  {
    user: 'testuser',
    pwd: 'testpass',
    roles: [{ role: 'readWrite', db: 'db_test' }],
  },
);
db.createCollection('users');
