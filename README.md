# README

This boilerplate configures a **dockerized** base application with:
- Ruby 3.1.1
- Rails 7.0.2
- MongoDB 4.4.3
- RSpec 3.11.0
- Articles scaffold (removable)

# Setup
If you have permission issues try running first this command in your local machine:
`$ sudo chown -R $USER:$USER .`

1. Build the application
`$ make build`

2. Start the application
`$ make up`

8. The [index page](http://localhost:3000/articles) should be up and running now!

# Create scaffold
`$ docker-compose run app rails g scaffold anentityname afieldname:afieldtype anotherfieldname:anotherfieldtype`

# Run tests
`$ make test`
